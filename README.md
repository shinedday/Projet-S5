# Projet S5

######### Parie 1 : contenue du dossier ##############
-> README
-> Un dossier avec plusieurs jeu de capteur
-> Une archive ZIP du code source
-> Un executable (le simulateur) .jar
-> Un executable (l'interface utilisateur) .jar
-> Une archive ZIP import avec les differentes librairies externes

######### Parie 2 : Lancement des executables ##############
0. info et pre-utilisation
Le programe s'occupe tout seul de la gestion de la base de donnée.
 * Il crée une database du nom de capteur
 * Puis 2 tables : capteur et valeur dans la database.
Il faut les supprimer si la database / les 2 tables existent deja.

(uniquement modifiable dans le code)
Les clefs de connexion pour la bdd sont :
L'url est : jdbc:mysql://localhost:3306/
L'id est : root
Le Mdp est : "" (vide)

1. Lancer l'interface utilisateur
2. lancer le simulateur et choisir une liste de capteur

######### Partie 3 : information complementaire ###########
Onglet : A posteriori 
* l'affichage de plusieur capteur en simultaner provoque des affichages faux
* les séparateur accepté pour la date et l'heure sont : "/" "-" " " ":" "_"
	Par exemple : 15:03:2000 10_00 ou 15/03/2000 10:00 etc..
Onglet : Gestion Capteur
* Supprimer le capteur : supprime de la BDD
* supprimer les données du capteur : remet simplement a 0 toutes les données receuillie par ce dernier
Onglet : Gestion BDD
Toute les fonction ici supprime les table / la database Capteur. Si une database existe deja avec le nom capteur, il faut la sauvegarder avant !
 


######### Parie 3 : Version logiciel ##############
 * eclipse 2020-12
 * wamp 3.2.3 / Apache/2.4.46 (Win64) PHP/7.3.21 /  5.7.31 - MySQL /  5.0.2 - phpMyAdmin

