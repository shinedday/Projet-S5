package ongletGestionBDD;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import clefBDD.Clef;
import outilInterfaceGraphique.Outil;

public class MainGestionBdd extends JPanel {

	public MainGestionBdd() {
		JButton reinitialiserDatabase = Outil.initJButton("Reinitialiser la Database", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clef.requeteUpdateSQLGeneral("DROP DATABASE IF EXISTS capteur");
				Clef.requeteUpdateSQLGeneral("CREATE DATABASE capteur");
				initTable();
			}
		});
		
		JButton reinitialiserTable = Outil.initJButton("Reinitialiser les tables", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initTable();
			}
		});
		
		JButton reinitialiserData = Outil.initJButton("Supprimer les donn�es de toute les tables", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetData();
			}
		});

		Outil.initPanel(this);
		this.add(reinitialiserDatabase);
		this.add(reinitialiserTable);
		this.add(reinitialiserData);
	}

	private void initTable() {
		resetData();
		Clef.requeteUpdateSQL("DROP TABLE IF EXISTS capteur");
		Clef.requeteUpdateSQL("Create table capteur(\r\n" + "    nom Varchar(50) not null,\r\n"
				+ "    typeFluide varchar(50) not null,\r\n" + "    seuilMin FLOAT not null,\r\n"
				+ "    seuilMax FLOAT not null,    \r\n" + "    etage INTEGER not null,\r\n"
				+ "    batiment varchar(50) not null,\r\n" + "    emplacement varchar(50) not null,\r\n"
				+ "    actif TINYINT,\r\n" + "    CONSTRAINT pk_nom PRIMARY KEY(nom)\r\n" + ")");
	}

	private void resetData() {
		Clef.requeteUpdateSQL("DROP TABLE IF EXISTS valeur");
		Clef.requeteUpdateSQL("Create table valeur (\r\n" + "    val Float,\r\n" + "    dateData DATETIME,\r\n"
				+ "    nom varchar (50),\r\n" + "    constraint fk_nom foreign key(nom) references Capteur(nom)\r\n"
				+ ")");
	}
}
