package outilInterfaceGraphique;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

public abstract class Outil {

	public static JLabel labelGenerator(String chaine) {
		JLabel res = new JLabel(chaine);
		res.setAlignmentX(Component.LEFT_ALIGNMENT);
		res.setMaximumSize(new Dimension(200, 25));
		return res;
	}

	public static void initPanel(JPanel panel) {
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 20));
	}

	public static void initSplitPane(JSplitPane panel) {
		panel.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
		panel.setDividerLocation(800);
		panel.setOneTouchExpandable(false);
		panel.setEnabled(false);
	}

	public static void initComboBox(JComboBox<String> box) {
		box.addItem("No Selection");
		box.setSelectedItem("No Selection");
		box.setMaximumSize(new Dimension(200, 25));
		box.setAlignmentX(Component.LEFT_ALIGNMENT);
	}

	public static JTextField initTextField() {
		JTextField text = new JTextField();
		text.setAlignmentX(Component.LEFT_ALIGNMENT);
		text.setMaximumSize(new Dimension(200, 25));
		return text;
	}

	public static JPanel labelAndField(String label, JTextField text) {
		JLabel res = new JLabel(label);
		res.setMaximumSize(new Dimension(100, 25));
		text.setMaximumSize(new Dimension(100, 25));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel.add(res);
		panel.add(text);
		panel.setMaximumSize(new Dimension(200, 25));
		return panel;
	}
	
	public static JButton initJButton(String texte, ActionListener action) {
		JButton button = new JButton();
		button.setText(texte);
		button.setMaximumSize(new Dimension(200, 25));
		button.setAlignmentX(Component.LEFT_ALIGNMENT);
		button.addActionListener(action);
		return button;
	}
}
