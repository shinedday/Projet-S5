package interfaceGraphique;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ongletGestionBDD.MainGestionBdd;
import ongletGestionCapteur.MainGestionCapteur;
import ongletGraphe.MainOngletGraphe;
import ongletTempsReel.MainTempsReel;

public class Fenetre extends JFrame {
	private JTabbedPane tabbedPane;

	private JSplitPane panneauTempsReel = new MainTempsReel();
	private JSplitPane panneauGraphe = new MainOngletGraphe();
	private JSplitPane panneauGestionCapteur = new MainGestionCapteur();
	private JPanel panneauGestionBDD = new MainGestionBdd();

	public Fenetre() {
		// ############## choix des options principals de la fenetre##################
		this.setTitle("Application capteurs");
		this.setSize(1024, 768);
		this.setResizable(false);
		// fermeture de la fen�tre quand l'utilisateur appuie sur la croix
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// centrer la fen�tre sur l'�cran
		this.setLocationRelativeTo(null);

		this.getContentPane().setLayout(null);

		tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(0, 0, this.getWidth(), this.getHeight());

		// ajout des onglets
		tabbedPane.addTab("temps r�el", panneauTempsReel);
		tabbedPane.addTab("a posteriori", panneauGraphe);
		tabbedPane.addTab("gestion capteurs", panneauGestionCapteur);
		tabbedPane.addTab("gestion Base de donnee", panneauGestionBDD);

		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (tabbedPane.getSelectedComponent().equals(panneauGestionBDD)) {
					JOptionPane.showConfirmDialog(null, "A Utiliser uniquement avec un serveur inactif/eteint",
							"Confirmation BDD", JOptionPane.DEFAULT_OPTION);
				}
			}
		});

		this.getContentPane().add(tabbedPane);
	}
}
