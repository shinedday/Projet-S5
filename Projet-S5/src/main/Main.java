package main;

import clefBDD.Clef;
import interfaceGraphique.Fenetre;

public class Main extends Serveur {
	public static void main(String[] args) {
		// ########################### Partie BDD ###############################
		Clef.requeteUpdateSQLGeneral("CREATE DATABASE if not EXISTS capteur ");
		Clef.requeteUpdateSQL(
				"Create table if not EXISTS capteur(nom Varchar(50) not null, typeFluide varchar(50) not null,"
						+ " seuilMin FLOAT not null, seuilMax FLOAT not null, etage INTEGER not null, "
						+ "batiment varchar(50) not null, emplacement varchar(50) not null, actif TINYINT, CONSTRAINT pk_nom PRIMARY KEY(nom))");
		Clef.requeteUpdateSQL("Create table if not EXISTS valeur ( val Float, dateData DATETIME, nom varchar (50), "
				+ "constraint fk_nom foreign key(nom) references Capteur(nom))");
		
		// ########################### Partie serveur ###############################
		Serveur c = new Serveur();
		Thread t = new Thread(c);
		t.start();

		// ########################### Partie Client ################################
		Fenetre fenetre = new Fenetre();
		fenetre.setVisible(true);
	}
}