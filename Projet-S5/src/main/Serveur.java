package main;

import java.io.*;
import java.net.*;

import capteur.Capteur;
import clefBDD.Clef;

public class Serveur implements Runnable {
	private static final int PORT = 8952;
	Socket socket;
	ServerSocket server;

	public Serveur() {
		try {
			server = new ServerSocket(PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			while (!server.isClosed()) {
				socket = server.accept();
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							BufferedReader plec = new BufferedReader(new InputStreamReader(socket.getInputStream()));
							boolean socketOuvert = true;
							while (socketOuvert) {
								try {
									String input = plec.readLine();
									if (input != null) {

										traitementInput(input);
										System.out.println("Serveur : Message re�u : " + input);
									}
								} catch (SocketException se) {
									socketOuvert = false;
								}
							}
							socket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				t.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void traitementConnexion(String[] messageBrut) {
		Capteur capteur = new Capteur(messageBrut[2], messageBrut[1]);
		String requete = capteur.connexionCapteur();
		Clef.requeteUpdateSQL(requete);
	}

	private void traitementDeconnexion(String[] messageBrut) {
		Clef.requeteUpdateSQL("UPDATE capteur SET actif = 0 WHERE nom =\"" + messageBrut[1] + "\";");
	}

	private void traitementData(String[] messageBrut) {
		float data = Float.parseFloat(messageBrut[2]);
		Clef.requeteUpdateSQL("insert into valeur(val,dateData,nom) select " + data + ",CURRENT_TIMESTAMP(),\""
				+ messageBrut[1] + "\" where (SELECT actif FROM capteur WHERE nom = \"" + messageBrut[1] + "\") = 1");
	}

	private void traitementInput(String input) {
		String[] messageBrut = input.split(" ", 3);

		if (messageBrut[0].equals("Connexion")) {
			traitementConnexion(messageBrut);
			return;
		}

		if (messageBrut[0].equals("Deconnexion")) {
			traitementDeconnexion(messageBrut);
			return;
		}
		traitementData(messageBrut);
	}
}