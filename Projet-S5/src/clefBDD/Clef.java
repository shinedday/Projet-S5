package clefBDD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class Clef {
	private static String url = "jdbc:mysql://localhost:3306/capteur";
	private static String urlgeneral = "jdbc:mysql://localhost:3306/";
	private static String id = "root";
	private static String mdp = "";
	private static Connection con = connection();
	private static Connection conGeneral = connectionGen();

	private static Connection connection() {
		try {
			return DriverManager.getConnection(Clef.url, Clef.id, Clef.mdp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Connection connectionGen() {
		try {
			return DriverManager.getConnection(Clef.urlgeneral, Clef.id, Clef.mdp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ResultSet requeteDataSQL(String requete) {
		ResultSet rst = null;
		try {
			Statement stmt = Clef.con.createStatement();
			System.out.println("Requete data SQL : " + requete);
			rst = stmt.executeQuery(requete);
			return rst;
		} catch (SQLException e) {
			System.out.println("Requete data SQL : echec");
			e.printStackTrace();
		}
		return rst;
	}

	public static void requeteUpdateSQL(String requete) {
		try {
			Statement stmt = Clef.con.createStatement();
			System.out.println("Requete update SQL : " + requete);
			stmt.executeUpdate(requete);
		} catch (SQLException e) {
			System.out.println("Requete update SQL : echec");
			e.printStackTrace();
		}
	}

	public static void requeteUpdateSQLGeneral(String requete) {
		try {
			Statement stmt = Clef.conGeneral.createStatement();
			System.out.println("Requete update general SQL : " + requete);
			stmt.execute(requete);
		} catch (SQLException e) {
			System.out.println("Requete update general SQL : echec");
			e.printStackTrace();
		}
	}
}
