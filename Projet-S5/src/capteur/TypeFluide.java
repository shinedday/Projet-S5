package capteur;

public enum TypeFluide {
	// affichage, requeteBDD unit�, seuilmin, seuilmax
	EAU("Eau", "EAU", "m3", 0, 10), ELECTRICITE("El�ctricit�", "ELECTRICITE", "kWh", 10, 500),
	AIRCOMPRIME("Air comprim�", "AIRCOMPRIME", "m3/h", 0, 5), TEMPERATURE("Temp�rature", "TEMPERATURE", "�C", 17, 22),
	AUCUN("", "", "", 0, 0);

	private final String affichage;
	private final String requeteBDD;
	private final String unite;
	private final int seuilMin;
	private final int seuilMax;

	TypeFluide(String affichage, String requeteBDD, String unite, int seuilMin, int seuilMax) {
		this.affichage = affichage;
		this.requeteBDD = requeteBDD;
		this.unite = unite;
		this.seuilMin = seuilMin;
		this.seuilMax = seuilMax;
	}

	public static TypeFluide toFluide(String chaine) {
		if (chaine.equals("Eau") || chaine.equals("EAU")) {
			return TypeFluide.EAU;
		}
		if (chaine.equals("El�ctricit�") || chaine.equals("ELECTRICITE")) {
			return TypeFluide.ELECTRICITE;
		}
		if (chaine.equals("Air comprim�") || chaine.equals("AIRCOMPRIME")) {
			return TypeFluide.AIRCOMPRIME;
		}
		if (chaine.equals("Temp�rature") || chaine.equals("TEMPERATURE")) {
			return TypeFluide.TEMPERATURE;
		}
		return TypeFluide.AUCUN;
	}

	public String affichage() {
		return affichage;
	}

	public String unite() {
		return unite;
	}

	public float seuilMin() {
		return seuilMin;
	}

	public float seuilMax() {
		return seuilMax;
	}

	public String requeteBDD() {
		return requeteBDD;
	}
}
