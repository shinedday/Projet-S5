package capteur;

import clefBDD.Clef;

public class Capteur implements Comparable<Capteur> {
	private String nom;
	private float SeuilMin;
	private float SeuilMax;
	private TypeFluide fluide;
	private int etage;
	private String batiment;
	private String emplacement;
	private Boolean actif;

	public Capteur(String nom, TypeFluide type, int etage, String batiment, String emplacement) {
		this.etage = etage;
		this.batiment = batiment;
		this.emplacement = emplacement;
		this.nom = nom;
		this.fluide = type;
		this.SeuilMin = fluide.seuilMin();
		this.SeuilMax = fluide.seuilMax();
		this.actif = true;
	}

	// String to capteur. From Serveur.
	public Capteur(String chaine, String nom) {
		String[] detail = chaine.split(":");

		this.fluide = TypeFluide.toFluide(detail[0]);
		this.nom = nom;
		this.batiment = detail[1];
		this.etage = Integer.parseInt(detail[2]);
		this.emplacement = detail[3];
		this.SeuilMin = this.fluide.seuilMin();
		this.SeuilMax = this.fluide.seuilMax();
		this.actif = true;
	}

	public String connexionCapteur() {
		String chaine = "\"" + nom + "\",";
		chaine += "\"" + fluide.requeteBDD() + "\",";
		chaine += SeuilMin + ",";
		chaine += SeuilMax + ",";
		chaine += etage + ",";
		chaine += "\"" + batiment + "\",";
		chaine += "\"" + emplacement + "\",";
		chaine += "1";

		String instruction = "INSERT INTO capteur(nom,typeFluide,seuilMin,seuilMax,etage,batiment,emplacement,actif)";
		instruction += "VALUES(" + chaine + ") ";
		instruction += "ON DUPLICATE KEY UPDATE actif=1 ";
		instruction += " and etage = " + etage;
		instruction += " and batiment = \"" + batiment + "\"";
		instruction += " and emplacement = \"" + emplacement + "\"";
		instruction += " and typeFluide = " + "\"" + fluide.requeteBDD() + "\";";

		return instruction;
	}

	public String getNom() {
		return nom;
	}

	public float getSeuilMin() {
		return SeuilMin;
	}

	public float getSeuilMax() {
		return SeuilMax;
	}

	public TypeFluide getFluide() {
		return fluide;
	}

	public int getEtage() {
		return etage;
	}

	public String getBatiment() {
		return batiment;
	}

	public String getEmplacement() {
		return emplacement;
	}

	public Boolean getActif() {
		return actif;
	}

	public void setSeuilMin(float seuilMin) {
		SeuilMin = seuilMin;
		String requete = "UPDATE capteur " + "SET seuilMin = " + SeuilMin + " " + "WHERE nom =\"" + nom + "\";";
		Clef.requeteUpdateSQL(requete);
	}

	public void setSeuilMax(float seuilMax) {
		SeuilMax = seuilMax;
		String requete = "UPDATE capteur " + "SET seuilMax = " + seuilMax + " " + "WHERE nom =\"" + nom + "\";";
		Clef.requeteUpdateSQL(requete);
	}

	public void setActif(Boolean actif) {
		this.actif = actif;
		String requete = "UPDATE capteur SET actif = ";
		if (actif) {
			requete += "1 ";
		} else {
			requete += "0 ";
		}
		requete += "WHERE nom =\"" + nom + "\";";
		Clef.requeteUpdateSQL(requete);
	}

	public int compareTo(Capteur o) {
		return this.nom.compareTo(((Capteur) o).nom);
	}
}
