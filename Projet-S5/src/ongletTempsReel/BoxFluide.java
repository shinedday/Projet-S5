package ongletTempsReel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

import capteur.TypeFluide;
import outilInterfaceGraphique.Outil;

public class BoxFluide extends JComboBox<String> {
	public BoxFluide() {
		Outil.initComboBox(this);
		this.addItem(TypeFluide.EAU.affichage());
		this.addItem(TypeFluide.AIRCOMPRIME.affichage());
		this.addItem(TypeFluide.ELECTRICITE.affichage());
		this.addItem(TypeFluide.TEMPERATURE.affichage());
		this.addActionListener(new CapteurListener());
	}

	private class CapteurListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			((MainTempsReel) BoxFluide.this.getParent().getParent())
			.actionFluide(TypeFluide.toFluide((String) BoxFluide.this.getSelectedItem()));
		}
	}
}
