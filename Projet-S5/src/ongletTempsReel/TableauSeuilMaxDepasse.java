package ongletTempsReel;

import java.awt.Color;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import clefBDD.Clef;

public class TableauSeuilMaxDepasse extends DefaultTableCellRenderer {

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		String nomCapteur = (String) table.getModel().getValueAt(row, 0);
		float valeur = Float.parseFloat(((String) table.getModel().getValueAt(row, 5)));
		if (isInRange(nomCapteur, valeur)) {
			setBackground(Color.RED);
			setForeground(Color.WHITE);
		} else {
			setBackground(table.getBackground());
			setForeground(table.getForeground());
		}
		return this;
	}
	
	private boolean isInRange(String capteur, float valeur) {
		ResultSet rst = Clef.requeteDataSQL("SELECT seuilMin, seuilMax FROM capteur WHERE nom=\"" + capteur + "\"");
		try {
			while (rst.next()) {
				return valeur<rst.getFloat("seuilMin") || valeur>rst.getFloat("seuilMax");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
