package ongletTempsReel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JComboBox;

import clefBDD.Clef;
import outilInterfaceGraphique.Outil;

public class BoxEtage extends JComboBox<String> {
	public BoxEtage(String batiment) {
		Outil.initComboBox(this);
		if (batiment == "") {
			this.setEnabled(false);
		} else {
			this.setEnabled(true);
			for (String etage : requeteSQL(batiment)) {
				this.addItem(etage);
			}
		}
		this.addActionListener(new CapteurListener());
	}

	private class CapteurListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			((MainTempsReel) BoxEtage.this.getParent().getParent())
					.actionEtage(String.valueOf(BoxEtage.this.getSelectedItem()));
		}
	}

	private Set<String> requeteSQL(String batiment) {
		Set<String> res = new HashSet<>();
		ResultSet rst = Clef.requeteDataSQL("SELECT etage FROM capteur WHERE batiment =\"" + batiment + "\"");
		try {
			while (rst.next()) {
				res.add(rst.getString("etage"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}
}
