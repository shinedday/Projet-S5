package ongletTempsReel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.Timer;

import capteur.TypeFluide;
import outilInterfaceGraphique.Outil;

public class MainTempsReel extends JSplitPane {
	private TypeFluide fluide;
	private String batiment;
	private String etage;
	private JScrollPane scrollPane;
	private JPanel filtres;

	public MainTempsReel() {
		fluide = TypeFluide.AUCUN;
		batiment = "No Selection";
		etage = "No Selection";
		filtres = new PartieDroite();

		constructeur();
		this.setRightComponent(filtres);

		ActionListener refleshTable = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				constructeur();
			}
		};
		new Timer(1000, refleshTable).start();
	}

	private void constructeur() {
		Tableau tableau = new Tableau(batiment, etage, fluide);
		JTable table = new JTable(tableau);
		table.setDefaultRenderer(Object.class, new TableauSeuilMaxDepasse());

		if (scrollPane != null) {
			scrollPane.removeAll();
		}
		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 5));
		scrollPane.repaint();
		scrollPane.revalidate();
		Outil.initSplitPane(this);
		this.setLeftComponent(scrollPane);
	}

	public void actionEtage(String etage) {
		this.etage = etage;
		constructeur();
	}

	public void actionFluide(TypeFluide fluide) {
		this.fluide = fluide;
		constructeur();
	}

	public void actionBatiment(String batiment) {
		this.batiment = batiment;
		constructeur();
	}
}
