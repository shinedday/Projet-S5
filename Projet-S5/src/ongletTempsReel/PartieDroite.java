package ongletTempsReel;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import outilInterfaceGraphique.Outil;

public class PartieDroite extends JPanel {
	private JComboBox<String> boxFluide;
	private JComboBox<String> boxBatiment;
	private JComboBox<String> boxEtage;

	public PartieDroite() {
		boxFluide = new BoxFluide();
		boxBatiment = new BoxBatiment();
		constructeur("");
	}

	private void constructeur(String batiment) {
		boxEtage = new BoxEtage(batiment);

		Outil.initPanel(this);
		this.add(Outil.labelGenerator("Filtre :"));
		this.add(Outil.labelGenerator("  Type de fluide :"));
		this.add(boxFluide);
		this.add(Outil.labelGenerator("  Localisation :"));
		this.add(Outil.labelGenerator("    Batiment :"));
		this.add(boxBatiment);
		this.add(Outil.labelGenerator("    Etage :"));
		this.add(boxEtage);
	}

	public void actionBatiment(String batiment) {
		this.removeAll();
		constructeur(batiment);
		this.repaint();
		this.revalidate();
		((MainTempsReel) PartieDroite.this.getParent()).actionBatiment(batiment);
	}
}
