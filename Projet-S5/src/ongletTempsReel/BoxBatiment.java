package ongletTempsReel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComboBox;

import clefBDD.Clef;
import outilInterfaceGraphique.Outil;

public class BoxBatiment extends JComboBox<String> {

	public BoxBatiment() {
		Outil.initComboBox(this);
		for (String batiment : requeteSQL()) {
			this.addItem(batiment);
		}
		this.addActionListener(new CapteurListener());
	}

	private class CapteurListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			((PartieDroite) BoxBatiment.this.getParent())
					.actionBatiment(String.valueOf(BoxBatiment.this.getSelectedItem()));
		}
	}

	private Set<String> requeteSQL() {
		Set<String> res = new HashSet<>();
		ResultSet rst = Clef.requeteDataSQL("SELECT batiment FROM capteur");
		try {
			while (rst.next()) {
				res.add(rst.getString("batiment"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
}
