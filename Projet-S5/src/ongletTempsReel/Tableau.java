package ongletTempsReel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import capteur.Capteur;
import capteur.TypeFluide;
import clefBDD.Clef;

public class Tableau extends AbstractTableModel {

	private List<Capteur> capteurs;
	private final String[] entete = { "Nom", "Fluide", "B�timent", "Etage", "Emplacement", "Valeur" };

	public Tableau(String batiment, String etage, TypeFluide fluide) {
		capteurs = new ArrayList<>();
		capteurs.addAll(requeteSQLContructionTableau(batiment, etage, fluide));
		Collections.sort(capteurs);
	}

	private Set<Capteur> requeteSQLContructionTableau(String batiment, String etage, TypeFluide fluide) {
		Set<Capteur> res = new HashSet<>();
		ResultSet rst = Clef.requeteDataSQL("SELECT nom, typeFluide, etage, batiment, emplacement FROM capteur "
				+ construtionCondition(batiment, etage, fluide) + "ORDER BY nom");
		try {
			while (rst.next()) {
				Capteur capteur = new Capteur(rst.getString("nom"), TypeFluide.toFluide((rst.getString("typeFluide"))),
						rst.getInt("etage"), rst.getString("batiment"), rst.getString("emplacement"));
				res.add(capteur);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	private String construtionCondition(String batiment, String etage, TypeFluide fluide) {
		if (batiment.equals("No Selection") && etage.equals("No Selection") && fluide.equals(TypeFluide.AUCUN)) {
			return "";
		}
		String chaine = " WHERE ";
		if (!batiment.equals("No Selection")) {
			chaine += "batiment = \"" + batiment + "\" ";
		}
		if (!etage.equals("No Selection")) {
			if (!batiment.equals("No Selection")) {
				chaine += "And ";
			}
			chaine += "etage = \"" + etage + "\" ";
		}
		if (!fluide.equals(TypeFluide.AUCUN)) {
			if (!batiment.equals("No Selection") || !etage.equals("No Selection")) {
				chaine += "And ";
			}
			chaine += "typeFluide = \"" + fluide.requeteBDD() + "\" ";
		}
		return chaine;
	}

	public int getRowCount() {
		return capteurs.size();
	}

	public int getColumnCount() {
		return entete.length;
	}

	public Object getValueAt(int indiceLigne, int indiceColonne) {
		Capteur capteur = capteurs.get(indiceLigne);
		switch (indiceColonne) {
		case 0:
			return capteur.getNom();
		case 1:
			return capteur.getFluide().affichage();
		case 2:
			return capteur.getBatiment();
		case 3:
			return String.valueOf(capteur.getEtage());
		case 4:
			return capteur.getEmplacement();
		case 5:
			Float tmp = requeteSQLDerniereValeur(capteur.getNom());
			return tmp.toString();
		}
		return null;
	}

	public String getColumnName(int indiceColonne) {
		return entete[indiceColonne];

	}

	public Class<?> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	private float requeteSQLDerniereValeur(String nomCapteur) {
		ResultSet rst = Clef
				.requeteDataSQL("SELECT val FROM valeur WHERE nom = '" + nomCapteur + "' ORDER BY dateData DESC");
		try {
			if (rst.next()) {
				return rst.getFloat("val");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
