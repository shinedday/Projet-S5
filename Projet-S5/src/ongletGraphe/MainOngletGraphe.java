package ongletGraphe;

import java.sql.Timestamp;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.jfree.chart.ChartPanel;

import capteur.TypeFluide;
import outilInterfaceGraphique.Outil;

public class MainOngletGraphe extends JSplitPane {
	private String capteur1;
	private String capteur2;
	private String capteur3;
	private TypeFluide fluide;
	private Timestamp debut;
	private Timestamp fin;

	private ChartPanel chart;
	private JPanel partieDroite;

	public MainOngletGraphe() {
		capteur1 = "";
		capteur2 = "";
		capteur3 = "";
		fluide = TypeFluide.AUCUN;
		debut = null;
		fin = null;

		partieDroite = new PartieDroite();
		constructeur();

	}

	private void constructeur() {
		chart = new Graphe(fluide, capteur1, capteur2, capteur3, debut, fin).getCp();
		Outil.initSplitPane(this);
		this.setLeftComponent(chart);
		this.setRightComponent(partieDroite);
	}

	public void actionCapteur(String capteur1, String capteur2, String capteur3) {
		this.capteur1 = capteur1;
		this.capteur2 = capteur2;
		this.capteur3 = capteur3;

		this.removeAll();
		constructeur();
		this.repaint();
		this.revalidate();
	}

	public void actionFluide(TypeFluide fluide) {
		this.fluide = fluide;

		this.removeAll();
		constructeur();
		this.repaint();
		this.revalidate();
	}

	public void actionButton(Date dateDebut, Date dateFin) {
		if (dateDebut == null || dateFin == null || dateDebut.after(dateFin) || dateDebut.equals(dateFin)) {
			debut = null;
			fin = null;
		} else {
			debut = new java.sql.Timestamp(dateDebut.getTime());
			fin = new java.sql.Timestamp(dateFin.getTime());
		}
		this.removeAll();
		constructeur();
		this.repaint();
		this.revalidate();
	}
}
