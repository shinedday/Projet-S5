package ongletGraphe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import capteur.TypeFluide;
import outilInterfaceGraphique.Outil;

public class BoxFluides extends JComboBox<String> {
	public BoxFluides(TypeFluide fluide) {
		Outil.initComboBox(this);
		this.addItem(TypeFluide.EAU.affichage());
		this.addItem(TypeFluide.AIRCOMPRIME.affichage());
		this.addItem(TypeFluide.ELECTRICITE.affichage());
		this.addItem(TypeFluide.TEMPERATURE.affichage());

		if (fluide == null) {
			this.setSelectedItem("No Selection");
		} else {
			this.setSelectedItem(fluide.affichage());
		}
		this.addActionListener(new FluideListener());
	}

	private class FluideListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			((PartieDroite) BoxFluides.this.getParent())
					.actionFluide(TypeFluide.toFluide((String) BoxFluides.this.getSelectedItem()));
		}
	}
}
