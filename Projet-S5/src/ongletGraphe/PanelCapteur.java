package ongletGraphe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import capteur.TypeFluide;
import clefBDD.Clef;
import outilInterfaceGraphique.Outil;

public class PanelCapteur extends JPanel {
	private JComboBox<String> boxCapteur1;
	private String capteur1;

	private JComboBox<String> boxCapteur2;
	private String capteur2;

	private JComboBox<String> boxCapteur3;
	private String capteur3;

	private TypeFluide fluide;

	public PanelCapteur(TypeFluide nouveauFluide) {
		capteur1 = "";
		capteur2 = "";
		capteur3 = "";
		fluide = nouveauFluide;

		constructeur();
	}

	private void constructeur() {
		ArrayList<String> capteurs = new ArrayList<>();

		if (fluide != null) {
			capteurs = requeteSQLNomsCapteurs(fluide);
		}
		boxCapteur1 = new BoxCapteur(chaineCapteurs(capteurs, capteur3, capteur2), capteur1);
		boxCapteur2 = new BoxCapteur(chaineCapteurs(capteurs, capteur3, capteur1), capteur2);
		boxCapteur3 = new BoxCapteur(chaineCapteurs(capteurs, capteur1, capteur2), capteur3);

		Outil.initPanel(this);
		this.add(Outil.labelGenerator("Selection de capteur :"));
		this.add(boxCapteur1);
		this.add(boxCapteur2);
		this.add(boxCapteur3);
	}

	public void actionCapteur() {
		capteur1 = String.valueOf(boxCapteur1.getSelectedItem());
		capteur2 = String.valueOf(boxCapteur2.getSelectedItem());
		capteur3 = String.valueOf(boxCapteur3.getSelectedItem());

		this.removeAll();
		constructeur();
		this.revalidate();
		this.repaint();

		((MainOngletGraphe) this.getParent().getParent()).actionCapteur(capteur1, capteur2, capteur3);
	}

	// permet de ne pas avoir des capteurs deja selectionné
	private ArrayList<String> chaineCapteurs(ArrayList<String> chaine, String capteur1, String capteur2) {
		ArrayList<String> capteurs = new ArrayList<>(chaine);
		capteurs.remove(capteur1);
		capteurs.remove(capteur2);
		return capteurs;
	}

	private ArrayList<String> requeteSQLNomsCapteurs(TypeFluide fluide) {
		ArrayList<String> res = new ArrayList<>();
		ResultSet rst = Clef.requeteDataSQL("SELECT nom FROM capteur WHERE typeFluide=\"" + fluide.requeteBDD() + "\"");
		try {
			while (rst.next()) {
				res.add(rst.getString("nom"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
}
