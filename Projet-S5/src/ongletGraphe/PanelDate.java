package ongletGraphe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import outilInterfaceGraphique.Outil;

public class PanelDate extends JPanel {
	private JTextField dateD;
	private JTextField heureD;

	private JTextField dateF;
	private JTextField heureF;

	private JButton button;

	public PanelDate() {
		dateD = Outil.initTextField();
		heureD = Outil.initTextField();

		dateF = Outil.initTextField();
		heureF = Outil.initTextField();

		button = Outil.initJButton("Valider", new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				actionButton();
			}
		});

		Outil.initPanel(this);
		this.add(Outil.labelGenerator("Date de debut :"));
		this.add(Outil.labelAndField("JJ/MM/AAAA", dateD));
		this.add(Outil.labelAndField("Heure:Minute", heureD));

		this.add(Outil.labelGenerator("Date de fin :"));
		this.add(Outil.labelAndField("JJ/MM/AAAA", dateF));
		this.add(Outil.labelAndField("Heure:Minute", heureF));

		this.add(button);
	}

	public void actionButton() {
		Date dateDebut = findDate(dateD.getText(), heureD.getText());
		Date dateFin = findDate(dateF.getText(), heureF.getText());
		((MainOngletGraphe) this.getParent().getParent()).actionButton(dateDebut, dateFin);
	}

	private Date findDate(String chainedate, String chaineheure) {
		String[] testFormattingSeparator = { ":", "/", "-", "_", " " };
		for (String separateurDate : testFormattingSeparator) {
			for (String separateurTime : testFormattingSeparator) {
				String chaineFormatter = "dd" + separateurDate + "MM" + separateurDate + "yyyyy HH" + separateurTime
						+ "mm";
				SimpleDateFormat formatter = new SimpleDateFormat(chaineFormatter);
				try {
					return formatter.parse(chainedate + " " + chaineheure);
				} catch (ParseException e) {

				}
			}
		}
		return null;
	}
}
