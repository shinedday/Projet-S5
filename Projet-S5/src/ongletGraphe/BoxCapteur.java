package ongletGraphe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;

import outilInterfaceGraphique.Outil;

public class BoxCapteur extends JComboBox<String> {
	public BoxCapteur(ArrayList<String> capteurs, String valeurCourante) {
		Outil.initComboBox(this);
		if (capteurs.isEmpty()) {
			this.setEnabled(false);
		} else {
			this.setEnabled(true);
			for (String capteur : capteurs) {
				this.addItem(capteur);
			}
		}
		if (valeurCourante != "") {
			this.setSelectedItem(valeurCourante);
		}
		this.addActionListener(new CapteurListener());
	}

	private class CapteurListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			((PanelCapteur) BoxCapteur.this.getParent()).actionCapteur();
		}
	}
}
