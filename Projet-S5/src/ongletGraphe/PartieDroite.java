package ongletGraphe;

import javax.swing.JPanel;

import capteur.TypeFluide;
import outilInterfaceGraphique.Outil;

public class PartieDroite extends JPanel {
	public PartieDroite() {
		construction(TypeFluide.AUCUN);
	}

	private void construction(TypeFluide fluide) {
		JPanel panelCapteur = new PanelCapteur(fluide);
		JPanel panelDate = new PanelDate();

		Outil.initPanel(this);
		this.add(Outil.labelGenerator("Selection d'un Fluide"));
		this.add(new BoxFluides(fluide));
		this.add(panelCapteur);
		this.add(panelDate);
	}

	public void actionFluide(TypeFluide fluide) {
		this.removeAll();
		construction(fluide);
		this.repaint();
		this.revalidate();
		((MainOngletGraphe) this.getParent()).actionFluide(fluide);
	}
}
