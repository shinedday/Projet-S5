package ongletGraphe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import javax.swing.BorderFactory;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import capteur.TypeFluide;
import clefBDD.Clef;

public class Graphe {
	private JFreeChart chart;
	private ChartPanel cp;

	private Float mintrouve = null;
	private Float maxtrouve = null;

	public Graphe(TypeFluide fluide, String capteur1, String capteur2, String capteur3, Timestamp debut,
			Timestamp fin) {
		CategoryDataset d = requeteSQLDataCapteur(debut, fin, capteur1, capteur2, capteur3);

		chart = ChartFactory.createLineChart("Evolution de : " + fluide.affichage(), "Date", fluide.unite(), d,
				PlotOrientation.VERTICAL, true, true, false);
		if (mintrouve != null && maxtrouve != null) {
			CategoryPlot test = (CategoryPlot) chart.getPlot();
			NumberAxis rangeAxis = (NumberAxis) test.getRangeAxis();
			rangeAxis.setRange(mintrouve - 1, maxtrouve + 1);
		}

		cp = new ChartPanel(chart, true);
		getCp().setBorder(BorderFactory.createEmptyBorder(5, 5, 50, 5));
	}

	private CategoryDataset requeteSQLDataCapteur(Timestamp debut, Timestamp fin, String... capteurs) {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (String capteur : capteurs) {
			ResultSet rst = Clef
					.requeteDataSQL("SELECT * FROM valeur WHERE nom=\"" + capteur + "\"" + chaineDate(debut, fin));
			try {
				while (rst.next()) {
					if (mintrouve == null) {
						mintrouve = rst.getFloat("val");
						maxtrouve = rst.getFloat("val");
					} else {
						if (rst.getFloat("val") < mintrouve) {
							mintrouve = rst.getFloat("val");
						}

						if (rst.getFloat("val") > maxtrouve) {
							maxtrouve = rst.getFloat("val");
						}
					}
					dataset.setValue(rst.getFloat("val"), capteur, new Long(rst.getTimestamp("dateData").getTime()));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return dataset;
	}

	private String chaineDate(Timestamp debut, Timestamp fin) {
		if (debut == null || fin == null) {
			return "";
		}
		return " AND dateData>=\'" + debut.toString() + "\' AND  dateData<=\'" + fin.toString() + "\'";
	}

	public ChartPanel getCp() {
		return cp;
	}
}
