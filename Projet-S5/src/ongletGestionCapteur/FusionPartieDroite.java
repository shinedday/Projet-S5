package ongletGestionCapteur;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import capteur.Capteur;
import outilInterfaceGraphique.Outil;

public class FusionPartieDroite extends JPanel {
	public FusionPartieDroite(Capteur capteur) {
		JTextArea descriptionCapteur = new DescriptionCapteur(capteur);
		JPanel descriptionSeuil = new DescriptionSeuil(capteur);
		JPanel optionAvance = new OptionAvance(capteur);

		Outil.initPanel(this);
		this.add(descriptionCapteur);
		this.add(descriptionSeuil);
		this.add(optionAvance);
	}
}
