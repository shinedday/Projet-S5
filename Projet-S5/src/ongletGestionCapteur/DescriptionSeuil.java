package ongletGestionCapteur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import capteur.Capteur;
import outilInterfaceGraphique.Outil;

public class DescriptionSeuil extends JPanel {
	private JTextField texteinputMin;
	private JTextField texteinputMax;

	private Capteur capteur;

	public DescriptionSeuil(Capteur capteur) {
		this.capteur = capteur;
		JButton button = Outil.initJButton("Valider", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionButton();
			}
		});
		
		texteinputMin = Outil.initTextField();
		texteinputMax = Outil.initTextField();

		Outil.initPanel(this);
		this.add(Outil.labelGenerator("Seuil min : " + capteur.getSeuilMin() + " " + capteur.getFluide().unite()));
		this.add(texteinputMin);
		this.add(Outil.labelGenerator("Seuil max : " + capteur.getSeuilMax() + " " + capteur.getFluide().unite()));
		this.add(texteinputMax);
		this.add(button);
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 100, 0));
	}

	public void actionButton() {
		String chaineSeuilMin = texteinputMin.getText();
		String chaineSeuilMax = texteinputMax.getText();

		if (chaineSeuilMin == "" || chaineSeuilMin == null || chaineSeuilMin.isEmpty()) {
			capteur.setSeuilMin(capteur.getFluide().seuilMin());
		} else {
			capteur.setSeuilMin(Float.valueOf(chaineSeuilMin));
		}
		if (chaineSeuilMax == "" || chaineSeuilMax == null || chaineSeuilMax.isEmpty()) {
			capteur.setSeuilMax(capteur.getFluide().seuilMax());
		} else {
			capteur.setSeuilMax(Float.valueOf(chaineSeuilMax));
		}
	}
}
