package ongletGestionCapteur;

import java.awt.Dimension;

import javax.swing.JTextArea;

import capteur.Capteur;

public class DescriptionCapteur extends JTextArea {
	public DescriptionCapteur(Capteur capteur) {
		String chaine = "Description : \n" + "  Nom :\n" + "    " 
				+ capteur.getNom() + "\n" 
				+ "  Typefluide:\n"
				+ "     " + capteur.getFluide().affichage() + "\n" 
				+ "\n" 
				+ "  Localisation :\n" 
				+ "    Batiment : "+ capteur.getBatiment() + "\n" 
				+ "    Etage : " + capteur.getEtage() + "\n" 
				+ "    Lieu : " + capteur.getEmplacement() + "\n" 
				+ "\n" 
				+ "  Actif : " + capteur.getActif();

		this.setText(chaine);
		this.setEditable(false);
		this.setHighlighter(null);
		this.setMaximumSize(new Dimension(200, 200));
		this.setAlignmentX(LEFT_ALIGNMENT);
	}
}
