package ongletGestionCapteur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import clefBDD.Clef;

public class ArbreDesCapteurs {
	private JTree jt;
	private JScrollPane arbreDesCapteurs;

	public ArbreDesCapteurs() {
		DefaultMutableTreeNode UT3 = constructionDelarbre();
		jt = new JTree(UT3);
		expandAllNodes(jt, 0, jt.getRowCount());
		jt.addTreeSelectionListener(new InterationUtilisateurArbre());
		arbreDesCapteurs = new JScrollPane(jt, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	}

	private DefaultMutableTreeNode constructionDelarbre() {
		DefaultMutableTreeNode UT3 = new DefaultMutableTreeNode("UT3");

		Set<String> listeBatiment = requeteSQLBatiment();
		for (String batiment : listeBatiment) {
			DefaultMutableTreeNode batimentActuel = new DefaultMutableTreeNode(batiment);
			UT3.add(batimentActuel);

			// requete listes etages
			Set<Integer> listeEtages = requeteSQLEtage(batiment);
			for (int etage : listeEtages) {
				DefaultMutableTreeNode etageActuel = new DefaultMutableTreeNode(etage);
				batimentActuel.add(etageActuel);

				// requete listes capteurs
				Set<String> listeCapteur = requeteSQLCapteur(batiment, etage);
				for (String capteur : listeCapteur) {
					etageActuel.add(new DefaultMutableTreeNode(capteur));
				}
			}
		}
		return UT3;
	}

	private void expandAllNodes(JTree tree, int startingIndex, int rowCount) {
		for (int i = startingIndex; i < rowCount; ++i) {
			tree.expandRow(i);
		}
		if (tree.getRowCount() != rowCount) {
			expandAllNodes(tree, rowCount, tree.getRowCount());
		}
	}

	private Set<String> requeteSQLBatiment() {
		Set<String> res = new HashSet<>();
		ResultSet rst = Clef.requeteDataSQL("SELECT batiment FROM capteur");
		try {
			while (rst.next()) {
				res.add(rst.getString("batiment"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	private Set<Integer> requeteSQLEtage(String batiment) {
		Set<Integer> res = new HashSet<>();
		ResultSet rst = Clef.requeteDataSQL("SELECT etage FROM capteur WHERE batiment = \"" + batiment + "\"");
		try {
			while (rst.next()) {
				res.add(rst.getInt("etage"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	private Set<String> requeteSQLCapteur(String batiment, int etage) {
		Set<String> res = new HashSet<>();
		ResultSet rst = Clef
				.requeteDataSQL("SELECT nom FROM capteur WHERE batiment = \"" + batiment + "\" and etage = " + etage);
		try {
			while (rst.next()) {
				res.add(rst.getString("nom"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	private class InterationUtilisateurArbre implements TreeSelectionListener {
		public void valueChanged(TreeSelectionEvent event) {
			DefaultMutableTreeNode selected_node = (DefaultMutableTreeNode) jt.getLastSelectedPathComponent();
			if (selected_node != null && selected_node.isLeaf()) {
				String chaine = selected_node.toString();
				((MainGestionCapteur) arbreDesCapteurs.getParent()).actionArbre(chaine);
			}
		}
	}

	public JScrollPane getArbreDesCapteurs() {
		return arbreDesCapteurs;
	}
}
