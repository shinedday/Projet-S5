package ongletGestionCapteur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import capteur.Capteur;
import clefBDD.Clef;
import outilInterfaceGraphique.Outil;

public class OptionAvance extends JPanel {
	public OptionAvance(Capteur capteur) {
		JButton suppressionCapteur = Outil.initJButton("Supprimer Le Capteur", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clef.requeteUpdateSQL("DELETE from valeur WHERE nom = \"" + capteur.getNom() + "\"");
				Clef.requeteUpdateSQL("DELETE from capteur WHERE nom = \"" + capteur.getNom() + "\"");
			}
		});
		
		JButton resetData = Outil.initJButton("Supprimer data", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clef.requeteUpdateSQL("DELETE from valeur WHERE nom = \"" + capteur.getNom() + "\"");
			}
		});
		
		JButton refreshTree = Outil.initJButton("Refresh l'arbre", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((MainGestionCapteur) OptionAvance.this.getParent().getParent()).actionRefresh();;
			}
		});

		Outil.initPanel(this);
		this.add(Outil.labelGenerator("Options avanc�es :"));
		this.add(suppressionCapteur);
		this.add(resetData);
		this.add(Outil.labelGenerator("Autre :"));
		this.add(refreshTree);
	}
}