package ongletGestionCapteur;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import capteur.Capteur;
import capteur.TypeFluide;
import clefBDD.Clef;
import outilInterfaceGraphique.Outil;

public class MainGestionCapteur extends JSplitPane {
	private JScrollPane arbreDesCapteurs;

	public MainGestionCapteur() {
		arbreDesCapteurs = new ArbreDesCapteurs().getArbreDesCapteurs();
		constructeur(new Capteur("", TypeFluide.AUCUN, 0, "", ""));
	}

	private void constructeur(Capteur capteur) {
		JPanel descriptionCapteur = new FusionPartieDroite(capteur);
		Outil.initSplitPane(this);
		this.setLeftComponent(arbreDesCapteurs);
		this.setRightComponent(descriptionCapteur);
	}

	private Capteur findCapteur(String nom) {
		Capteur res = new Capteur("", TypeFluide.AUCUN, 0, "", "");
		ResultSet rst = Clef.requeteDataSQL("SELECT * FROM capteur WHERE nom = \"" + nom + "\"");
		try {
			while (rst.next()) {
				TypeFluide type = TypeFluide.toFluide(rst.getString("typeFluide"));
				res = new Capteur(rst.getString("nom"), type, rst.getInt("etage"), rst.getString("batiment"),
						rst.getString("emplacement"));

				res.setActif(rst.getInt("actif") == 1);
				res.setSeuilMin(rst.getFloat("seuilMin"));
				res.setSeuilMax(rst.getFloat("seuilMax"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public void actionArbre(String chaine) {
		Capteur capteur = findCapteur(chaine);
		this.removeAll();
		constructeur(capteur);
		this.repaint();
		this.revalidate();
	}

	public void actionRefresh() {
		this.removeAll();
		arbreDesCapteurs = new ArbreDesCapteurs().getArbreDesCapteurs();
		constructeur(new Capteur("", TypeFluide.AUCUN, 0, "", ""));
		this.repaint();
		this.revalidate();
	}
}
